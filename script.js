document.getElementById('fav_color').addEventListener('change', (e) => {
	let your_color_radio = document.getElementById('your_color');
	your_color_radio.checked = true;
	let fav_color = document.getElementById('fav_color');
	fav_color.value =  e.target.value;
}); 

function createGrid(grid_length, squares_count) {
	let grid = document.createElement('div');
	for (let i = 0; i < squares_count; i++) {
		grid.appendChild(createRow(grid_length, squares_count));
	}

	return grid;
}

function createRow(grid_length, squares_count) {
	let row = document.createElement('div');
	for (let i = 0; i < squares_count; i++) {
		row.appendChild(createSquare(Math.floor(grid_length / squares_count) - 2));
	}
	row.setAttribute('style', 'height: ' + Math.floor(grid_length / squares_count) +'px;'); 

	return row;
}

function createSquare(side) {
	let square = document.createElement('div');
	square.setAttribute('class', 'square');
	square.setAttribute('style', 'color: white; border: 1px solid black; display: inline-block; width: ' + side + 'px; height: ' + side + 'px;');
	square.addEventListener('mouseenter', function() {
		let your_color_radio = document.getElementById('your_color');
		if (your_color_radio.checked) {
			let fav_color = document.getElementById('fav_color');
			this.style.background = fav_color.value;
		}
		else { this.style.background = getRandomColor(); }
        
    });

	return square;
}

function startAgain() {
	let squares_input = document.getElementById('squares_count');
	let squares_count = parseInt(squares_input.value);
	if ((squares_count > 64) || (squares_count < 10)) {
		alert("Grid length can't be greater than 64 and less than 10!");
		return false;
	}
	else { clearGrid(squares_count); }
}

function clearGrid(squares_count) {
	let grid_div = document.getElementById('grid');
	grid_div.innerHTML = '';
	grid_div.appendChild(createGrid(grid_div.offsetWidth,squares_count));
}

function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }

    return color;
}

(() => {
	let grid_div = document.getElementById('grid');
	grid_div.setAttribute('style', 'width: ' + (window.innerHeight - 250) + 'px; margin: 30px auto;');
	startAgain();
})();
